import sys
import os
import time


class FME_WS_proc(object):

    def __init__(self, fme_py_path, fme_workspace, parameters):
        self.fme_py_path = fme_py_path
        self.fme_workspace = fme_workspace
        self.parameters = parameters


    def workspace_exec(self, filename):
        # Import fmeobjects python package
        sys.path.append('C:\\apps\\FME\\fmeobjects\\python27')
        sys.path.append('C:\\apps\\FME')
        os.chdir('C:\\apps\\FME')
        import fmeobjects

        start_time = time.time()

        #Run workspace
        try:
            runner = fmeobjects.FMEWorkspaceRunner()
            runner.runWithParameters(self.fme_workspace,self.parameters)
            running = None

            end_time = time.time()
            return 'Done with: ' + str(filename) + '\t' + str(end_time - start_time) + ' s'

        except fmeobjects.FMEException as ex:
            return ex.message






if __name__ == '__main__':
    fme_py_path = r'C:\\apps\\FME\\fmeobjects\\python27'
    fme_workspace = r'H:\Arbete\IT_GIS_SBF\SHP_Test\xyz2esrishape.fmw'
    parameters = {}
    parameters['SourceDataset_XYZ'] = r"H:\FME\NNH2010_xyz\62125_3575_25.xyz"
    parameters['DestDataset_ESRISHAPE'] = "H:\Arbete\IT_GIS_SBF\SHP_Test"
    filename = '62125_3575_25.xyz'

    f = FME_WS_proc(fme_py_path, fme_workspace, parameters)
    msg = f.workspace_exec(filename)
    print msg