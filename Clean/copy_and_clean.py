import arcpy
import sys
import time


class clean_copy_class(object):
    def __init__(self, all_clients, output, translate_dict):
        self.all_clients = all_clients
        self.output = output
        self.translate_dict = translate_dict

    def clean_copy_mod(self):
        start_time = time.time()
        # Create final GDB for all rasters
        try:
            arcpy.CreateFileGDB_management(self.output, 'output_final.gdb')
            final_gdb = self.output + '\\output_final.gdb'
        except Exception:
            e = sys.exc_info()[1]
            print e.args[0]

        #Copy rasters to output_final.gdb and delete scratch_ip.gdb and output_ip.gdb.
        try:
            for client in self.all_clients:
                ip = client[0].replace('.', '_')
                port = client[1]
                client_out_GDB = self.output + '\\' + 'output' + '_' + ip + '.gdb'
                client_scratch_GDB = self.output + '\\' + 'scratch' + '_' + ip + '.gdb'
                if arcpy.Exists(client_out_GDB):
                    for dirpath, dirnames, filenames in arcpy.da.Walk(client_out_GDB):
                        for filename in filenames:
                            arcpy.CopyRaster_management(client_out_GDB + '\\' + filename,
                                                        final_gdb + '\\' + self.translate_dict[filename])

                    arcpy.Delete_management(client_scratch_GDB)
                    arcpy.Delete_management(client_out_GDB)

                else:
                    print 'Could not find FGDB: ' + str(client_out_GDB)
                    continue
        except Exception:
            e = sys.exc_info()[1]
            print e.args[0]

        end_time = time.time()
        print 'Time: ' + str(end_time - start_time) + 's'


if __name__ == '__main__':
    output = r'H:\Arbete\MF\Solkartan\Test\Data\Output'
    all_clients = [('10.81.40.149', 60492), ('10.81.29.212', 59348)]
    cu = clean_copy_class(all_clients, output)
    cu.clean_copy_mod()