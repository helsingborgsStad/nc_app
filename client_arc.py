import socket
import sys
import time
import arcpy
from ARC import client_arcpy_processing


class Client(object):
    def __init__(self):
        script_path = sys.path[0]
        with open(script_path + '\\StartUp_params.txt') as f:
            self.start_params = dict(x.rstrip().split('=') for x in f)
        self.server_host = self.start_params['server_host']
        self.server_port = int(self.start_params['server_port'])
        self.arc_function_name = self.start_params['arc_function']
        self.socket = None
        self.output = ''
        self.output_DB = ''
        self.scratch_DB = ''

    def socket_create(self):
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error as e:
            print "Socket creation error: " + str(e)
            return
        return

    def socket_connect(self):
        try:
            self.socket.connect((self.server_host, self.server_port))
            self.output = self.socket.recv(1024)
        except socket.error as e:
            print("Socket connection error: " + str(e))
            time.sleep(5)
            raise

        try:
            # Create output and scratch database for client
            FGDB_suffix = '_' + self.socket.getsockname()[0].replace('.', '_') + '.gdb'
            arcpy.CreateFileGDB_management(self.output, 'output' + FGDB_suffix)
            arcpy.CreateFileGDB_management(self.output, 'scratch' + FGDB_suffix)
            self.output_DB = self.output + '\\' + 'output' + FGDB_suffix
            self.scratch_DB = self.output + '\\' + 'scratch' + FGDB_suffix
            print 'Ready'
        except:
            print "Error in creating client FGDBs"

        return

    def receive_commands(self):
        while True:
            try:
                self.socket.send('Ready for calc')
                data = self.socket.recv(1024)
                filename, filepath = data.split('|')
                print 'Calculating: ' + str(filename)
                calc = client_arcpy_processing.raster_processing(filename, filepath, self.output_DB, self.scratch_DB)
                calc_status = getattr(calc, self.arc_function_name)()
                print calc_status
            except socket.error as e:
                if e[0] == 10054:
                    print 'Failed: Connection lost'
                    break
                else:
                    continue


def main():
    client = Client()
    client.socket_create()
    client.socket_connect()
    client.receive_commands()

if __name__ == '__main__':
    main()
