import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

handler = logging.FileHandler('hello.log', mode='w')
handler.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

logger.addHandler(handler)

logger.info('Start script...')

a = ['abcdef', 1234]
b = []

logger.debug('Values: %s', a)

for item in a:
    try:
        b.append(int(item))
        logger.info('Success with value: %s', item)
    except:
        logger.error('Failed with value: %s', item)