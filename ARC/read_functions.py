import inspect
import client_arcpy_processing


class read_avail_funcs(object):

    def __init__(self):
        self.raster_functions = inspect.getmembers(client_arcpy_processing.raster_processing,
                                      predicate=inspect.ismethod)
        self.avail_funcs = []

    def get_names(self):
        for func in self.raster_functions:
            if func[0] != '__init__':
                self.avail_funcs.append(func[0])
        return self.avail_funcs


if __name__=='__main__':
    Read = read_avail_funcs()
    avail_funcs = Read.get_names()


