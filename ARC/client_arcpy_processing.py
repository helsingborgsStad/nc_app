import arcpy
from arcpy import env
import sys
import random

if arcpy.CheckExtension("Spatial") == "Available":
    arcpy.CheckOutExtension("Spatial")
    from arcpy.sa import *
else:
    print 'Cannot enable spatial analyst on client.'


class raster_processing(object):
    def __init__(self, filename, filepath, output_db, scratch_db):
        self.filename = filename
        self.filepath = filepath
        self.output_db = output_db
        self.scratch_db = scratch_db

    def slope(self):
        try:
            arcpy.env.workspace = self.output_db
            arcpy.env.scratchWorkspace = self.scratch_db
            arcpy.env.overwriteOutput = True
            out_slope = Slope(self.filepath, "DEGREE")
            out_slope.save(self.output_db + '\\' + self.filename)

            return 'Done with: ' + str(self.filename)
        except Exception:
            e = sys.exc_info()[1]
            return e.args[0]

    def solar_area_helsingborg(self):
        try:
            arcpy.env.workspace = self.output_db
            arcpy.env.scratchWorkspace = self.scratch_db
            arcpy.env.overwriteOutput = True
            out_global_radiation = AreaSolarRadiation(self.filepath,
                                                      time_configuration=TimeMultipleDays(2016, 1, 365),
                                                      day_interval=14,
                                                      hour_interval=0.5,
                                                      calculation_directions=32,
                                                      diffuse_proportion=0.4,
                                                      transmittivity=0.5)
            out_global_radiation.save(self.output_db + '\\' + self.filename)

            return 'Done with ' + str(self.filename)
        except Exception:
            e = sys.exc_info()[1]
            return e.args[0]


def test_main():
    test_layers = [r'\\R002345\flygfoto\laserdata\DEM_1m_inklusive_byggnader\DEM_1m_inklusive_byggnader.gdb\_101551_6210786']
    proc_layers = []
    s = 'abcdefghijklmnopqrstuvxyz'
    for layer in test_layers:
        ran_word = ''.join(random.choice(s) for i in range(6))
        proc_layers.append(ran_word + '|' + layer)

    output = r'H:\Arbete\MF\Solkartan\Test\Data\Output\output1.gdb'
    scratch_DB = r'H:\Arbete\MF\Solkartan\Test\Data\Output\scratch1.gdb'

    print proc_layers

    for file in proc_layers:
        filename, filepath = file.split('|')
        print 'Calculating: ' + filename
        ras_proc = raster_processing(filename, filepath, output, scratch_DB)
        calc_status = ras_proc.solar_area_helsingborg()
        print calc_status

if __name__ == '__main__':
    test_main()
