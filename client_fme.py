import sys
import socket
import time
from FME import FME_processing


class Client(object):
    def __init__(self):
        script_path = sys.path[0]
        with open(script_path + '\\StartUp_params.txt') as f:
            self.start_params = dict(x.rstrip().split('=') for x in f)

        self.server_host = self.start_params['server_host']
        self.server_port = int(self.start_params['server_port'])
        self.fme_py_path = self.start_params['fme_py_path']
        self.fme_workspace = self.start_params['fme_workspace']
        self.fme_in_type = self.start_params['fme_in_type']
        self.fme_dest_type = self.start_params['fme_dest_type']
        self.fme_dest_path = self.start_params['fme_dest_path']
        self.socket = None

    def socket_create(self):
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error as e:
            print "Socket creation error: " + str(e)
            return
        return

    def socket_connect(self):
        try:
            self.socket.connect((self.server_host, self.server_port))
            self.output = self.socket.recv(1024)
            print 'Ready'
        except socket.error as e:
            print("Socket connection error: " + str(e))
            time.sleep(5)
            raise
        return

    def receive_commands(self):
        while True:
            try:
                self.socket.send('Ready for calc')
                data = self.socket.recv(1024)
                filename, filepath = data.split('|')
                print 'Calculating: ' + str(filename)
                parameters = {}
                parameters[self.fme_in_type] = filepath
                parameters[self.fme_dest_type] = self.output
                print parameters
                work_obj = FME_processing.FME_WS_proc(self.fme_py_path, self.fme_workspace, parameters)
                calc_status = work_obj.workspace_exec(filename)
                print calc_status
            except socket.error as e:
                if e[0] == 10054:
                    print 'Failed: Connection lost'
                    break
                else:
                    continue

def main():
    client = Client()
    client.socket_create()
    client.socket_connect()
    client.receive_commands()

if __name__ == '__main__':
    main()
