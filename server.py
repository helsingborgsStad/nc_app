import SocketServer
import os
import socket
import sys
import threading
from time import localtime, strftime
from Clean import copy_and_clean
from LoadLayers import load_layers1

all_clients = []

COMMANDS = {'help': ['\t\tShows this help'],
            'list': ['\t\tLists connected clients'],
            'layers_left': ['\tPrints the number of layers left for calculations'],
            'load_GDB_layers': ['Loads layer-references in specified ESRI GDB'],
            'load_other_layers': ['Loads all layer-references in a specified folder'],
            'start_calc': ['\tStarts sending layer-references to connected clients'],
            'stop_calc': ['\tStops sending layer-references to connected clients'],
            'clean_up': ['\tCopy calculated files to output_final.gdb and deletes temporary FGDBs'],
            'shutdown': ['\tShuts server down']
            }


class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        # Store connected clients
        global all_clients
        all_clients.append(self.client_address)
        print self.client_address, 'connected'

        # Send output path to client
        self.request.send(output)

        while True:
            try:
                while start_calc == 1:
                    client_status = self.request.recv(1024)
                    if client_status == 'Ready for calc':
                        if len(proc_layers) > 0:
                            self.request.send(proc_layers.pop())
                    else:
                        continue
            except:
                continue


class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass


def start_net_processing(server):
    print_help()
    while True:
        cmd = raw_input('--> ').lower()
        if cmd == 'list':
            list_connections(server)
        elif cmd == 'help':
            print_help()
        #elif cmd == 'load_GDB_layers':
            #global proc_layers
            #proc_layers, translate_dict = load_layers('GDB')
        #elif cmd == 'load_other_layers':
            #proc_layers = load_layers('FOLDER')
        elif cmd == 'layers_left':
            try:
                print len(proc_layers)
            except:
                print 'No layers imported'
        elif cmd == 'start_calc':
            start_stop_calc(1)
        elif cmd == 'stop_calc':
            start_stop_calc(0)
        elif cmd == 'clean_up':
            clean_up(translate_dict)
        elif cmd == 'shutdown':
            print 'Server shutdown'
            server.shutdown()
            server.server_close()
            sys.exit()
            break
        else:
            print 'Command not recognized'
            continue


def print_help():
    print '\n------- COMMANDS -------'
    print '________________________________________________________________________'
    for cmd, v in COMMANDS.items():
        print("{0}:\t{1}".format(cmd, v[0]))
    print '________________________________________________________________________'
    return


def list_connections(server):
    # global all_clients
    print '------- CONNECTED CLIENTS -------'
    print 'COUNT: ' + str(len(all_clients))
    for client in all_clients:
        print 'IP: ' + client[0] + '\tPort: ' + str(client[1])

def start_stop_calc(startStop):
    global start_calc
    start_calc = startStop

def load_layers(store_type):
    # Load layers from ESRI GDB
    if store_type == 'GDB':
        try:
            print 'Loading...'
            ld = load_layers1.LoadLayers(in_data)
            proc_layers, translate_dict = ld.DEM_w_buildings()
            print '...Done'
            print 'Example of loaded layer:\t' + proc_layers[0].split('|')[0]
            return proc_layers, translate_dict
        except:
            print 'Could not load layers'
            return
    #Load other layers
    elif store_type == 'FOLDER':
        try:
            print 'Loading...'
            #proc_layers = ['62125_3575_25.xyz|H:\\FME\\NNH2010_xyz\\62125_3575_25.xyz']
            ld = load_layers1.LoadLayers(fme_in_path)
            proc_layers = ld.other_layers()
            print '...Done'
            print 'Example of loaded layer:\t' + proc_layers[0].split('|')[0]
            return proc_layers
        except:
            print 'Could not load layers'
            return


def clean_up(translate_dict):
    # Create final GDB for all rasters
    print 'Copying rasters to output_final.gdb and cleaning FGDBs...'
    cu = copy_and_clean.clean_copy_class(all_clients, output, translate_dict)
    cu.clean_copy_mod()
    print '...Done'
    return


def set_start_params(server):
    # Automaticaly detects Host IP and prints into StartUp_params for client-connection.
    script_path = sys.path[0]
    host_conns = socket.gethostbyname_ex(socket.gethostname())[-1]
    if len(host_conns) == 1:
        server_host = host_conns[0]
    if len(host_conns) == 2:
        server_host = host_conns[1]

    # Read server_port
    server_port = server.server_address[1]

    # Read and write StartUp_params-file
    read_lines = []
    start_params = {}
    with open(script_path + '\\StartUp_params.txt', 'r') as f:
        for line in f.readlines():
            if 'server_host' in line:
                word_str, old_ip = line.rstrip().split('=')
                line = line.replace(old_ip, str(server_host))
                start_params[word_str] = str(server_host)
            elif 'server_port' in line:
                word_str, old_port = line.rstrip().split('=')
                line = line.replace(old_port, str(server_port))
                start_params[word_str] = str(server_port)
            else:
                key, value = line.rstrip().split('=')
                start_params[key] = value
            read_lines.append(line)


    with open(script_path + '\\StartUp_params.txt', 'w') as f:
        for line in read_lines:
            f.write(line)
            f.truncate()

    # Read output-path and make new directory at location
    start_output = start_params['output']
    current_time = strftime("%H_%M_%S", localtime())
    global output
    output = start_output + '\\' + 'output_' + current_time
    if not os.path.isdir(output):
        os.mkdir(output)

    global in_data
    in_data = start_params['in_data']
    global fme_in_path
    fme_in_path = start_params['fme_in_path']
    PORT = int(start_params['server_port'])
    HOST = ''
    global start_calc  # Governs weather clients should calculate layers or not. 0=Don't calc, 1=start calc
    start_calc = 0

    return server_host, server_port


def main(FME_or_ARC, Folder_or_GDB):
    server = ThreadedTCPServer(('', 0), ThreadedTCPRequestHandler)
    server_host, server_port = set_start_params(server)
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.daemon = True
    server_thread.start()
    print '\n\n-----------------------'
    print "Server loop running in thread:", server_thread.name
    print 'Server IP:\t' + str(server_host) + '\nServer Port:\t' + str(server_port)
    print '-----------------------\n\n'

    # Load layers at server start
    if Folder_or_GDB == 'GDB':
        global proc_layers
        global translate_dict
        proc_layers, translate_dict = load_layers('GDB')
    elif Folder_or_GDB == 'FOLDER':
        proc_layers = load_layers('FOLDER')

    server_thread2 = threading.Thread(target=start_net_processing(server))
    server_thread2.daemon = True
    server_thread2.start()



if __name__ == "__main__":
    main('FME', 'FOLDER')
