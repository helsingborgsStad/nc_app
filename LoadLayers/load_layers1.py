import arcpy
import random
import os


class LoadLayers(object):
    def __init__(self, in_data):
        self.in_data = in_data

    def randomword(self, length):
        s = 'abcdefghijklmnopqrstuvxyz'
        return ''.join(random.choice(s) for i in xrange(length))

    def DEM_w_buildings(self):
        try:
            arcpy.env.workspace = self.in_data
            raster_list = []
            translate_dict = {}
            s = 'abcdefghijklmnopqrstuvxyz'
            word_length = 6
            for dirpath, dirnames, filenames in arcpy.da.Walk(self.in_data):
                for filename in filenames:
                    if filename != "buildings_DEM":
                        # Create a random name for layer with length: word_length
                        alt_filename = ''.join(random.choice(s) for i in xrange(word_length))
                        translate_dict[alt_filename] = filename
                        raster_list.append(alt_filename + '|' + dirpath + '\\' + filename)

            return raster_list, translate_dict
        except:
            print 'Loading layers failed'
            return

    def other_layers(self):
        try:
            proc_layers = []
            for root, dirs, files in os.walk(self.in_data):
                for file in files:
                    proc_layers.append(file + '|' + root + '\\' + file)
            return proc_layers
        except:
            print 'Loading layers failed'
            return

if __name__ == '__main__':
    ld = LoadLayers(r'\\R002345\Flygfoto\laserdata\DEM 1m inklusive byggnader\buildingDEM_all.gdb')
    ld.DEM_w_buildings()
