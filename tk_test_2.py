import Tkinter as tk
import tkFileDialog as tkf
import Tkconstants
import ttk
import sys
import threading
import server
import client_arc
import client_fme
from ARC import read_functions

LARGE_FONT = ('Verdana', 15, 'bold')
MEDIUM_FONT = ('Verdana', 9)
SMALL_FONT = ('Verdana', 4)


class Server_client_UI(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        tk.Tk.iconbitmap(self, default='network-icon2.ico')
        tk.Tk.wm_title(self, 'Network Processing')

        container = tk.Frame(self)
        container.pack(side='top', fill='both', expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        for F in (StartPage,
                  ServerPage_FME1,
                  ServerPage_FME2,
                  ClientPage_FME,
                  ServerPage_Arc,
                  ClientPage_Arc):
            frame = F(container, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky='nsew')

        self.show_frame(StartPage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()


class StartPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        labelFrame1 = ttk.LabelFrame(self)
        labelFrame1.pack(side='left', fill='both', expand='yes')

        label1 = tk.Label(labelFrame1, text='FME', font=LARGE_FONT)
        label1.pack(fill=Tkconstants.BOTH)
        label1.place(relx=0.4, rely=0.2)

        button_server_FME = ttk.Button(labelFrame1, text='Server',
                                       command=lambda: controller.show_frame(ServerPage_FME1))

        button_server_FME.pack(padx= 5, pady= 5)
        button_server_FME.place(relx=0.35, rely=0.4)
        button_client_FME = ttk.Button(labelFrame1, text='Client',
                                       command=lambda: controller.show_frame(ClientPage_FME))
        button_client_FME.pack(padx= 5, pady= 5)
        button_client_FME.place(relx=0.35, rely=0.55)

        labelFrame2 = ttk.LabelFrame(self)
        labelFrame2.pack(side='right', fill='both', expand='yes')

        label2 = tk.Label(labelFrame2, text='ARC', font=LARGE_FONT)
        label2.pack(fill=Tkconstants.BOTH)
        label2.place(relx=0.4, rely=0.2)

        button_server_Arc = ttk.Button(labelFrame2, text='Server',
                                       command=lambda: controller.show_frame(ServerPage_Arc))

        button_server_Arc.pack(padx= 5, pady= 5)
        button_server_Arc.place(relx=0.35, rely=0.4)

        button_client_Arc = ttk.Button(labelFrame2, text='Client',
                                       command=lambda: controller.show_frame(ClientPage_Arc))
        button_client_Arc.pack(padx= 5, pady= 5)
        button_client_Arc.place(relx=0.35, rely=0.55)

class ServerPage_FME1(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        label1 = tk.Label(self, text='Server (FME)', font='LARGE_FONT')
        label1.pack(pady=10, padx=10)

        button1 = ttk.Button(self, text=u'\u21E6', width=3,
                             command=lambda: controller.show_frame(StartPage))
        button1.pack()
        button1.place(relx=0.02, rely=0.03)

        labelFrame1 = ttk.LabelFrame(self, text='How many readers?')
        labelFrame1.pack(fill='x', expand='yes', pady=10)
        e1 = ttk.Entry(labelFrame1, width=70)
        e1.pack(side='left', expand='yes')

        labelFrame2 = ttk.LabelFrame(self, text='How many writers?')
        labelFrame2.pack(fill='x', expand='yes', pady=10)
        e2 = ttk.Entry(labelFrame2, width=70)
        e2.pack(side='left', expand='yes')

        next_button = ttk.Button(self, text='Next',
                                       command=lambda: controller.show_frame(ServerPage_FME2))
        next_button.pack(pady=2, side='bottom')

class ServerPage_FME2(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label1 = tk.Label(self, text='Server (FME)', font='LARGE_FONT')
        label1.pack(pady=10, padx=10)

        button1 = ttk.Button(self, text=u'\u21E6', width=3,
                             command=lambda: controller.show_frame(ServerPage_FME1))
        button1.pack()
        button1.place(relx=0.02, rely=0.03)

        labelFrame1 = ttk.LabelFrame(self, text='FME workspace')
        labelFrame1.pack(fill='x', expand='yes', pady=10)
        e1 = ttk.Entry(labelFrame1, width=70)
        e1.pack(side='left', expand='yes')
        button3 = ttk.Button(labelFrame1, text='...', width=3,
                             command=lambda: askfile(self, e1, 'Choose FME workspace'))
        button3.pack(side='right', padx=10)

        labelFrame2 = ttk.LabelFrame(self, text='Input files folder or GDB', width=200)
        labelFrame2.pack(fill='x', expand='yes', pady=10)
        e2 = ttk.Entry(labelFrame2, width=70)
        e2.pack(side='left', expand='yes')
        button2 = ttk.Button(labelFrame2, text='...', width=3,
                             command=lambda: askdirectory(self, e2, 'Choose folder or GDB with input files'))
        button2.pack(side='right', padx=10)

        labelFrame3 = ttk.LabelFrame(self, text='Input type')
        labelFrame3.pack(fill='x', expand='yes', pady=10)
        e3 = ttk.Entry(labelFrame3, width=70)
        e3.pack(side='left', expand='yes')

        labelFrame4 = ttk.LabelFrame(self, text='Output folder', width=200)
        labelFrame4.pack(fill='x', expand='yes', pady=10)
        e4 = ttk.Entry(labelFrame4, width=70)
        e4.pack(side='left', expand='yes')
        button4 = ttk.Button(labelFrame4, text='...', width=3,
                             command=lambda: askdirectory(self, e4, 'Choose output folder'))
        button4.pack(side='right', padx=10)

        labelFrame5 = ttk.LabelFrame(self, text='Output type')
        labelFrame5.pack(fill='x', expand='yes', pady=10)
        e5 = ttk.Entry(labelFrame5, width=70)
        e5.pack(side='left', expand='yes')

        button5 = ttk.Button(self, text='Start Server',
                             command=lambda: StartServer('FME',
                                                         Arc_output=None,
                                                         Arc_in_data=None,
                                                         Arc_function=None,
                                                         FME_input_folder=e2.get(),
                                                         FME_workspace=e1.get(),
                                                         FMEInType=e3.get(),
                                                         FME_output_folder=e4.get(),
                                                         FMEOutType=e5.get()))
        button5.pack(pady=2, side='bottom')


class ClientPage_FME(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text='Client (FME)', font='LARGE_FONT')
        label.pack(pady=10, padx=10)

        button1 = ttk.Button(self, text=u'\u21E6', width=3,
                             command=lambda: controller.show_frame(StartPage))
        button1.pack(pady=30)
        button1.place(relx=0.02, rely=0.03)

        button2 = ttk.Button(self, text='Start Client',
                             command=lambda: StartClient('FME'))
        button2.pack(pady=20)


class ServerPage_Arc(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text='Server (Arc)', font='LARGE_FONT')
        label.pack(pady=10, padx=10)

        button1 = ttk.Button(self, text=u'\u21E6', width=3,
                             command=lambda: controller.show_frame(StartPage))
        button1.pack()
        button1.place(relx=0.02, rely=0.03)

        labelFrame1 = ttk.LabelFrame(self, text='Input files folder or GDB', width=200)
        labelFrame1.pack(fill='x', expand='yes', pady=10)
        e1 = ttk.Entry(labelFrame1, width=70)
        e1.pack(side='left', expand='yes')
        button2 = ttk.Button(labelFrame1, text='...', width=3,
                             command=lambda: askdirectory(self, e1, 'Choose folder or GDB with input files'))
        button2.pack(side='right', padx=10)

        function_names = get_function_names()
        labelFrame2 = ttk.LabelFrame(self, text='Function', width=200)
        labelFrame2.pack(fill='x', expand='yes', pady=10)
        function_combo = ttk.Combobox(labelFrame2, values=function_names, state='readonly', width=40)
        function_combo.current(0)
        function_combo.pack(padx=10, pady=5)

        labelFrame3 = ttk.LabelFrame(self, text='Output folder', width=200)
        labelFrame3.pack(fill='x', expand='yes', pady=10)
        e2 = ttk.Entry(labelFrame3, width=70)
        e2.pack(side='left', expand='yes')
        button3 = ttk.Button(labelFrame3, text='...', width=3,
                             command=lambda: askdirectory(self, e2, 'Choose output folder'))
        button3.pack(side='right', padx=10)

        button4 = ttk.Button(self, text='Start Server',
                             command=lambda: StartServer('ARC',
                                                         Arc_output=e2.get(),
                                                         Arc_in_data=e1.get(),
                                                         Arc_function=function_combo.get(),
                                                         FME_input_folder=None,
                                                         FME_workspace=None,
                                                         FMEInType=None,
                                                         FME_output_folder=None,
                                                         FMEOutType=None))

        button4.pack(pady=2, side='bottom')


class ClientPage_Arc(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = tk.Label(self, text='Client (Arc)', font='LARGE_FONT')
        label.pack(pady=10, padx=10)

        button1 = ttk.Button(self, text=u'\u21E6', width=3,
                             command=lambda: controller.show_frame(StartPage))
        button1.pack(pady=50)
        button1.place(relx=0.02, rely=0.03)

        button2 = ttk.Button(self, text='Start Client',
                             command=lambda: StartClient('ARC'))
        button2.pack(pady=20)


# Functions for above pages
def askfile(self, entryField, title):
    file_opt = options = {}
    options['defaultextension'] = '.fmw'
    options['filetypes'] = [('FME workspace', '.fmw'), ('all files', '.*')]
    options['initialdir'] = 'C:\\'
    # options['initialfile'] = 'myfile.txt'
    # options['parent'] = root
    options['title'] = title
    filename = tkf.askopenfilename(**file_opt)
    file_correct = filename.replace('/', '\\')
    entryField.delete(0, len(entryField.get()))
    entryField.insert(0, file_correct)
    return


def askdirectory(self, entryField, title):
    dir_opt = options = {}
    options['initialdir'] = 'C:\\'
    options['mustexist'] = False
    options['title'] = title
    directory = tkf.askdirectory(**dir_opt)
    dir_correct = directory.replace('/','\\')
    entryField.delete(0, len(entryField.get()))
    entryField.insert(0, dir_correct)
    return

def get_function_names():
    Reader = read_functions.read_avail_funcs()
    return Reader.get_names()

###################################################################

def SubmitParams(FME_or_ARC, Arc_output, Arc_in_data, Arc_function, FME_input_folder,
                 FME_workspace, FMEInType, FME_output_folder, FMEOutType):
    script_path = sys.path[0]

    User_params = {}
    User_params['output'] = Arc_output
    User_params['in_data'] = Arc_in_data
    User_params['arc_function'] = Arc_function
    User_params['fme_in_path'] = FME_input_folder
    User_params['fme_workspace'] = FME_workspace
    User_params['fme_in_type'] = FMEInType
    User_params['fme_dest_path'] = FME_output_folder
    User_params['fme_dest_type'] = FMEOutType

    if FME_or_ARC == 'FME':
        read_lines = []
        start_params = {}
        with open(script_path + '\\StartUp_params.txt', 'r') as f:
            for line in f.readlines():
                if 'output' in line:
                    word_str, old = line.rstrip().split('=')
                    line = line.replace(old, str(User_params['fme_dest_path']))
                    start_params[word_str] = str(User_params['fme_dest_path'])
                elif 'fme_in_path' in line:
                    word_str, old = line.rstrip().split('=')
                    line = line.replace(old, str(User_params['fme_in_path']))
                    start_params[word_str] = str(User_params['fme_in_path'])
                elif 'fme_workspace' in line:
                    word_str, old = line.rstrip().split('=')
                    line = line.replace(old, str(User_params['fme_workspace']))
                    start_params[word_str] = str(User_params['fme_workspace'])
                elif 'fme_in_type' in line:
                    word_str, old = line.rstrip().split('=')
                    line = line.replace(old, str(User_params['fme_in_type']))
                    start_params[word_str] = str(User_params['fme_in_type'])
                elif 'fme_dest_path' in line:
                    word_str, old = line.rstrip().split('=')
                    line = line.replace(old, str(User_params['fme_dest_path']))
                    start_params[word_str] = str(User_params['fme_dest_path'])
                elif 'fme_dest_type' in line:
                    word_str, old = line.rstrip().split('=')
                    line = line.replace(old, str(User_params['fme_dest_type']))
                    start_params[word_str] = str(User_params['fme_dest_type'])
                else:
                    key, value = line.rstrip().split('=')
                    start_params[key] = value
                read_lines.append(line)

    elif FME_or_ARC == 'ARC':
        read_lines = []
        start_params = {}
        with open(script_path + '\\StartUp_params.txt', 'r') as f:
            for line in f.readlines():
                if 'output' in line:
                    word_str, old = line.rstrip().split('=')
                    line = line.replace(old, str(User_params['output']))
                    start_params[word_str] = str(User_params['output'])
                elif 'in_data' in line:
                    word_str, old = line.rstrip().split('=')
                    line = line.replace(old, str(User_params['in_data']))
                    start_params[word_str] = str(User_params['in_data'])
                elif 'arc_function' in line:
                    word_str, old = line.rstrip().split('=')
                    line = line.replace(old, str(User_params['arc_function']))
                    start_params[word_str] = str(User_params['arc_function'])
                else:
                    key, value = line.rstrip().split('=')
                    start_params[key] = value
                read_lines.append(line)

    with open(script_path + '\\StartUp_params.txt', 'w') as f:
        for line in read_lines:
            f.write(line)
            f.truncate()

def StartServer(FME_or_ARC, Arc_output, Arc_in_data, Arc_function, FME_input_folder,
                FME_workspace, FMEInType, FME_output_folder, FMEOutType):

    start_params = SubmitParams(FME_or_ARC, Arc_output, Arc_in_data, Arc_function, FME_input_folder,
                                FME_workspace, FMEInType, FME_output_folder, FMEOutType)

    # Check if input is GDB or folder
    current_in_path = Arc_in_data or FME_input_folder
    print current_in_path
    if current_in_path[-1] == '/':
        Folder_or_GDB = 'FOLDER'
    elif current_in_path[-1] == '\\':
        Folder_or_GDB = 'FOLDER'
    elif current_in_path[-3:] == 'gdb':
        Folder_or_GDB = 'GDB'
    else:
        Folder_or_GDB = 'FOLDER'

    server_thread1 = threading.Thread(target=server.main(FME_or_ARC, Folder_or_GDB))
    server_thread1.daemon = True
    server_thread1.start()

def StartClient(FME_or_ARC):

    if FME_or_ARC == 'FME':
        client_thread1 = threading.Thread(target=client_fme.main)
        client_thread1.daemon = True
        client_thread1.start()

    elif FME_or_ARC == 'ARC':
        client_thread1 = threading.Thread(target=client_arc.main)
        client_thread1.daemon = True
        client_thread1.start()

def main():
    app = Server_client_UI()
    app.mainloop()


if __name__ == '__main__':
    main()
